﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dictionary_Console
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> options = new List<string> { "Car", "House", "Cat", "Dog", "Chicken", "Apple" };

            Console.WriteLine("Dictionary:");
            for (int i = 0; i < options.Count; ++i)
            {
                Console.WriteLine("" + (i + 1) + ". " + options[i]);
            }

            while (true)
            {
                try
                {
                    Console.WriteLine("Enter a string:");
                    string input = Console.ReadLine();
                    if (options.Contains(input))
                    {
                        Console.WriteLine(input + " is existing in dictionary");
                    }
                    else
                    {
                        options.Add(input);
                        Console.WriteLine("Added " + input + " to dictionary");
                    }
                }
                catch (Exception e)
                {
                    break;
                }
            }

            Console.WriteLine("Goodbye!");
            Console.ReadLine();

        }
    }
}

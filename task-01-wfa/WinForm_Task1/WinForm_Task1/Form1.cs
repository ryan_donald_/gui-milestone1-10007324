﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinForm_Task1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            radioButton1.Checked = true;
        }

        private void Convert_Button_Click(object sender, EventArgs e)
        {
            try
            {
                double distance = Convert.ToDouble(Value.Text);
                if (radioButton1.Checked)
                {
                    Result.Text = string.Format("Distance in Miles: {0}", (distance * 0.62137119));
                }
                else if (radioButton2.Checked)
                {
                    Result.Text = string.Format("Distance in KM: {0}", (distance / 0.62137119));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Critical Error: " + ex.Message);                
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Winform_Task4
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        string[] fruits = new string[] { "Apple","Banana","Orange","Peach","Pear","Cherry"};
        string[] vegitables = new string[] { "Tomato", "Red Pepper", "Onion", "Potato", "Carrot", "Pumpkin" }; 
        List<string> FruitsList = new List<string>();
        List<string> VegitablesList = new List<string>();
        bool Found = false;
        private void button1_Click(object sender, EventArgs e)
        {
            string Name;
            FruitsList.AddRange(fruits);
            Name = Fruits.Text;
            foreach (var item in FruitsList)
            {
                if (item == Name)
                {
                    Found = true;
                }
            }
            if (Found)
            {
                MessageBox.Show("Yes, It is in there");
                Found = false;
            }
            else if (!Found)
            {
                MessageBox.Show("No, This item is not in there");
            }
        }
        private void button2_Click(object sender, EventArgs e)
        {
            string Name = Vegitable.Text;
            VegitablesList.AddRange(vegitables);
            foreach (var item in VegitablesList)
            {
                if (item == Name)
                {
                    Found = true;
                }
            }
            if (Found)
            {
                MessageBox.Show("Yes, It is in there");
                Found = false;
            }
            else if (!Found)
            {
                MessageBox.Show("No, This item is not in there");
            }
        }
    }
}

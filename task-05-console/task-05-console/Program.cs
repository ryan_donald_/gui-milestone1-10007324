﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task5
{
    class Program
    {
        static void Main(string[] args)
        {
            Random RandomNumber = new Random();
            int score = 0;
            List<int> number = new List<int>();
            Console.WriteLine("=============== Mini Game ===================");
            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine("Enter Number Between ( 1 --> 5 ): ");
                int Number = Convert.ToInt32(Console.ReadLine());
                if (Number == RandomNumber.Next(1, 6))
                {
                    number.Add(Number);
                    score++;
                    score++;
                }
            }
            Console.WriteLine("============== Your Result =============== \n");
            Console.Write("Matched Numbers: ");
            foreach (var item in number)
            {
                Console.Write(string.Format("| {0} |", item));
            }
            Console.WriteLine("Your Scores (out of 10) : {0}", score);

            //Press any key 
            Console.WriteLine("Press any key to Continue ...");
            Console.ReadKey();
        }
    }
}

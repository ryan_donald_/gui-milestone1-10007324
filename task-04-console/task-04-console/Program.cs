﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task4
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] fruits = new string[] { "Apple", "Banana", "Orange", "Peach", "Pear", "Cherry" };
            string[] vegitables = new string[] { "Tomato", "Red Pepper", "Onion", "Potatoe", "Carrot", "Pumpkin" };
            string Name;
            List<string> Fruits = new List<string>();
            List<string> Vegitables = new List<string>();

            Fruits.AddRange(fruits);
            Vegitables.AddRange(vegitables);
            int Count = 0;
            Console.WriteLine("Select Options: ");
            Console.WriteLine("1) Fruit Name: ");
            Console.WriteLine("2) Vegitable Name: ");
            string option = Console.ReadLine();
            switch (option)
            {
                case "1":
                    Console.WriteLine("Enter any Fruit Name?");
                    Name = Console.ReadLine();
                    foreach (var item in Fruits)
                    {

                        if (item == Name)
                        {
                            Count++;
                            Console.WriteLine("Yes, It is in there");
                        }
                    }
                    break;
                case "2":
                    Console.WriteLine("Enter any Vegitable Name?");
                    Name = Console.ReadLine();
                    foreach (var item in Vegitables)
                    {
                        if (item == Name)
                        {
                            Count++;
                            Console.WriteLine("Yes, It is in there");
                            break;
                        }
                    }
                    break;
                default:
                    Console.WriteLine("Enter Correct Numeber ...");
                    break;
            }
            if (Count < 1)
            {
                Console.WriteLine("No, This item is not in there");
            }
            Console.WriteLine("Press any key to Continue ...");
            Console.ReadKey();
        }
    }
}

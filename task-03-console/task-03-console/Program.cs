﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication2
{
    class Program
    {
        static void Main(string[] args)
        {
            int Option;
            Console.WriteLine("=============== Change OutPut Console ===================");
            Console.WriteLine("Select Option: ");
            Console.WriteLine("1) for Changing Background Color: ");
            Console.WriteLine("2) for Beep Sound: ");
            Console.WriteLine("3) for Changing Forground Color: ");
            Console.WriteLine("4) for Exit");
            Option = Convert.ToInt32(Console.ReadLine());
            switch (Option)
            {
                case 1:
                    Console.BackgroundColor++;
                    break;
                case 2:
                    Console.Beep(3000, 3000);
                    break;
                case 3:
                    Console.ForegroundColor++;
                    Console.ForegroundColor++;
                    Console.ForegroundColor++;

                    break;
                default:
                    Console.WriteLine("Enter the Correct Number ...");
                    break;
            }

            Console.WriteLine("Press any key to Continue...");
            Console.ReadKey();
        }
    }
}

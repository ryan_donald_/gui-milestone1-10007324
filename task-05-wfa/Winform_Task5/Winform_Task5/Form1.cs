﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Winform_Task5
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        Random RandomNumber = new Random();
        int score = 0, i = 0;
        List<int> number = new List<int>();
        private void button1_Click(object sender, EventArgs e)
        {
            i++;
            Console.WriteLine("Enter Number Between ( 1 --> 5 ): ");
            int Number = Convert.ToInt32(textBox1.Text);
            if (Number == RandomNumber.Next(1, 6))
            {
                number.Add(Number);
                score++;
                score++;
                Scores.Text = score.ToString();
            }
            Scores.Text = score.ToString();
            if (i >= 6)
            {
                MessageBox.Show("Game Over:\n Your Total Scores: "+score);
                Application.Exit();
            }
            MessageBox.Show(string.Format("You Entered {0} Number.", i));
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            Scores.Text = score.ToString();
        }
    }
}

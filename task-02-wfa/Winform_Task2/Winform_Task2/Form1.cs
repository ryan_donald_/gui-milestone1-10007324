﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Winform_Task2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Convert_Button_Click(object sender, EventArgs e)
        {
            try
            {
                double InputAmount = Convert.ToInt64(Amount.Text);
                double amount = 0;
                Result.Text = string.Format("Total Amount(GST (x 1.15) ): {0}", (amount += (InputAmount * 1.15)));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Critical Error: " + ex.Message);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}

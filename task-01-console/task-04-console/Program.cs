﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Converter_Console
{
    class Program
    {
        static void Main(string[] args)
        {
            double km2mile = 0.621371;
            while (true)
            {
                try
                {
                    Console.WriteLine("KM <-> Mile Converter");
                    Console.WriteLine("1. KM -> Mile");
                    Console.WriteLine("2. Mile -> KM");
                    Console.WriteLine("3. Exit");

                    int option = Int32.Parse(Console.ReadLine());

                    if (option == 1)
                    {
                        Console.WriteLine("Enter distance in KM: ");
                        double input = Double.Parse(Console.ReadLine());
                        Console.WriteLine(input + " KM = " + (input * km2mile) + " miles");
                    }
                    else if (option == 2)
                    {
                        Console.WriteLine("Enter distance in miles: ");
                        double input = Double.Parse(Console.ReadLine());
                        Console.WriteLine(input + " milse = " + (input / km2mile) + " KM");
                    }
                    else
                    {
                        break;
                    }
                }
                catch (Exception e)
                {
                    break;
                }
            }

            Console.WriteLine("Goodbye!");
            Console.ReadLine();
        }
    }
}